package org.ultramine.mods.shops.te;

import com.mojang.authlib.GameProfile;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.server.MinecraftServer;
import org.ultramine.mods.shops.packets.PacketTEAdminShop;
import org.ultramine.mods.shops.packets.PacketTEShop;
import org.ultramine.server.util.InventoryUtil;

import java.util.UUID;

public class TileEntityAdminShop extends TileEntityShop
{
	public double tradeBalance;
	
	{
		productAmount = 60000;
	}
	
	@Override
	public void setOwner(GameProfile owner)
	{
		super.setOwner(new GameProfile(new UUID(0, 0), "Server"));
	}

	@Override
	public boolean isAdminShop()
	{
		return true;
	}

	@Override
	protected boolean ownerHasMoney(double amount)
	{
		return true;
	}

	@Override
	protected void ownerAddMoney(double amount)
	{
		tradeBalance += amount;
	}

	@Override
	protected void ownerSubtractMoney(double amount)
	{
		tradeBalance -= amount;
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public void updatePlaces(boolean notify)
	{
		places = 9999;
		if(notify)
			sendTileEntityPacket();
	}

	@SideOnly(Side.SERVER)
	@Override
	protected void transferToPlayer(EntityPlayerMP player, int amount)
	{
		ItemStack is = getProduct().copy();
		is.stackSize = amount;
		InventoryUtil.addItem(player, is);
		productAmount -= amount;
	}

	@SideOnly(Side.SERVER)
	@Override
	protected void transferToShop(EntityPlayerMP player, int amount)
	{
		ItemStack is = getProduct().copy();
		is.stackSize = amount;
		InventoryUtil.removeItem(player.inventory, is);
		productAmount += amount;
	}

	@SideOnly(Side.SERVER)
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		tradeBalance = nbt.getDouble("tb");
		productAmount = nbt.getInteger("pa");
	}

	@SideOnly(Side.SERVER)
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setDouble("tb", tradeBalance);
		nbt.setInteger("pa", productAmount);
	}

	@SideOnly(Side.SERVER)
	@Override // Server
	public Packet getDescriptionPacket()
	{
		return new PacketTEAdminShop(product, mode, sellPrice, buyPrice, maxSellPrice, maxBuyPrice, sellAmount, buyAmount, productAmount, places, "Server", tradeBalance)
			.form(this).toServerPacket();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void handlePacketClient(PacketTEShop p)
	{
		super.handlePacketClient(p);
		PacketTEAdminShop pa = (PacketTEAdminShop)p;
		tradeBalance = pa.tradeBalance;
	}

	@SideOnly(Side.SERVER)
	@Override // Server handled client command
	public void handlePacketServer(PacketTEShop p, EntityPlayerMP player)
	{
		boolean isOwner = MinecraftServer.getServer().getConfigurationManager().func_152596_g(player.getGameProfile());
		if(isOwner && p.places == 4)
			productAmount	= p.productAmount;
		super.handlePacketServer(p, player);
	}
}
