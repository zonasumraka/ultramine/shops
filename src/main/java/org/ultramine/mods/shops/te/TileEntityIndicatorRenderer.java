package org.ultramine.mods.shops.te;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.ultramine.util.I18n.tlt;

public class TileEntityIndicatorRenderer extends TileEntitySpecialRenderer
{
	private static final NumberFormat dformat = new DecimalFormat("#0.##");
	
	public void renderTileEntityAt(TileEntity par1TileEntity, double par2, double par4, double par6, float par8)
	{
		this.renderTileEntityIndicatorAt((TileEntityIndicator)par1TileEntity, par2, par4, par6, par8);
	}
	
	public void renderTileEntityIndicatorAt(TileEntityIndicator tile, double par2, double par4, double par6, float par8)
	{
		TileEntity ent = tile.getWorldObj().getTileEntity(tile.xCoord, tile.yCoord-1, tile.zCoord);
		if(ent == null || !(ent instanceof TileEntityShop))
			return;
		TileEntityShop shop = (TileEntityShop)ent;
		
		GL11.glColor3f(1f, 1f, 1f);
		GL11.glPushMatrix();
		float var10 = 0.6666667F;
		float var12;
		
		var12 = 0.0F;
		switch(tile.getWorldObj().getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord))
		{
		case 3: var12 = 90.0F;	break;
		case 1: var12 = -90.0F;	break;
		case 0: var12 = 180.0F;	break;
		case 2: 				break;
		}

		GL11.glTranslatef((float)par2 + 0.5F, (float)par4 + 0.75F * var10, (float)par6 + 0.5F);
		GL11.glRotatef(-var12, 0.0F, 1.0F, 0.0F);
		//GL11.glTranslatef(0.0F, -0.3125F, -0.4375F);
		GL11.glTranslatef(0.0F, -0.3125F, -0.4160F);
		
		GL11.glPushMatrix();
		GL11.glScalef(var10, -var10, -var10);
		GL11.glPopMatrix();
		FontRenderer fr = func_147498_b();
		var12 = 0.016666668F * var10;
		GL11.glTranslatef(0.0F, 0.5F * var10, 0.07F * var10);
		GL11.glScalef(var12, -var12, var12);
		GL11.glNormal3f(0.0F, 0.0F, -1.0F * var12);
		GL11.glDepthMask(false);
		
		String prodName = null;
		if(shop.getProduct() != null)
		{
			String prodName1 = shop.getProduct().getItem().getItemStackDisplayName(shop.getProduct());
			if(prodName1 != null)
				prodName = prodName1;
		}
		
		if(prodName == null)
			prodName = tlt("ultramine.shop.guibuy.none");
		
		String Sell	= tlt("ultramine.shop.indicator.sell");
		String Bay	= tlt("ultramine.shop.indicator.buy");
		String SellC = "";
		String SellK = "";
		String BuyC = "";
		String BuyK = "";
		
		boolean canSell = shop.canSell(1);
		boolean canBuy = shop.canBuy(1);
		
		if(canSell)
		{
			SellC = tlt("ultramine.shop.indicator.inprice", dformat.format(shop.getCurrentSellPrice()), shop.getClientMoneySign());
			SellK = tlt("ultramine.shop.indicator.incount", shop.getMaxAmountForSell());
		}
		if(canBuy)
		{
			BuyC = tlt("ultramine.shop.indicator.inprice", dformat.format(shop.getCurrentBuyPrice()), shop.getClientMoneySign());
			BuyK = tlt("ultramine.shop.indicator.incount", shop.getMaxAmountForBuy());
		}
		
		int color = 0xffffff;
		byte lines = 8;
		byte mod = 0;
		
		String prodName1 = fr.trimStringToWidth(prodName, 80);
		if(prodName1.length() != prodName.length())
		{
			String prodName2 = prodName.substring(prodName1.length());
			mod = 12;
			lines = 9;
			fr.drawString(prodName1, -fr.getStringWidth(prodName1) / 2, 0 * 10 - lines * 5+2, 0xFF5555);
			fr.drawString(prodName2, -fr.getStringWidth(prodName2) / 2, 1 * 10 - lines * 5+2, 0xFF5555);
		}
		else
		{
			fr.drawString(prodName, -fr.getStringWidth(prodName) / 2, 0 * 10 - lines * 5, 0xFF5555);
		}
		
		if(!canSell)
		{
			fr.drawString(tlt("ultramine.shop.indicator.not") + " " + Sell, -40, 1 * 10 - lines * 5+mod, color);
		}
		else
		{
			fr.drawString(Sell,		-40,	1 * 10 - lines * 5+mod, color);
			fr.drawString(SellC,	-40,	2 * 10 - lines * 5+mod, color);
			fr.drawString(SellK,	-40,	3 * 10 - lines * 5+mod, color);
		}
		if(!canBuy)
		{
			fr.drawString(tlt("ultramine.shop.indicator.not") + " " + Bay, -40, 5 * 10 - lines * 5+mod, color);
		}
		else
		{
			fr.drawString(Bay,	-40,	5 * 10 - lines * 5+mod, color);
			fr.drawString(BuyC,	-40,	6 * 10 - lines * 5+mod, color);
			fr.drawString(BuyK,	-40,	7 * 10 - lines * 5+mod, color);
		}
		//var17.drawString(SellMsg, -var17.getStringWidth(SellMsg) / 2, 1 * 10 - lines * 5, var13);
		//var17.drawString(BayMsg, -var17.getStringWidth(BayMsg) / 2, 2 * 10 - lines * 5, var13);
		//var17.drawString(var15, -var17.getStringWidth(var15) / 2, 3 * 10 - lines * 5, var13);

		GL11.glDepthMask(true);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glPopMatrix();
	 }
}
