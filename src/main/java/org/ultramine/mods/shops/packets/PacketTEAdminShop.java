package org.ultramine.mods.shops.packets;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;

import java.io.IOException;

public class PacketTEAdminShop extends PacketTEShop
{
	public double tradeBalance;
	
	public PacketTEAdminShop(){}
	public PacketTEAdminShop(ItemStack item, byte mode, double sellPrice, double buyPrice, double maxSellPrice, double maxBuyPrice,
			int sellCount, int buyCount, int productCount, int places, String owner, double tradeBalance)
	{
		super(item, mode, sellPrice, buyPrice, maxSellPrice, maxBuyPrice, sellCount, buyCount, productCount, places, owner);
		this.tradeBalance = tradeBalance;
	}
	
	@Override
	public void readPacketData(PacketBuffer buf) throws IOException
	{
		super.readPacketData(buf);
		tradeBalance = buf.readDouble();
	}
	
	@Override
	public void writePacketData(PacketBuffer buf) throws IOException
	{
		super.writePacketData(buf);
		buf.writeDouble(tradeBalance);
	}
}
