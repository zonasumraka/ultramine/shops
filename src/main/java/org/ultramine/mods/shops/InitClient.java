package org.ultramine.mods.shops;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.ultramine.mods.shops.economy.ClientEconomy;
import org.ultramine.mods.shops.te.TileEntityAdminShop;
import org.ultramine.mods.shops.te.TileEntityIndicator;
import org.ultramine.mods.shops.te.TileEntityIndicatorRenderer;
import org.ultramine.mods.shops.te.TileEntityShop;
import org.ultramine.mods.shops.te.TileEntityVetrinoRenderer;

@SideOnly(Side.CLIENT)
public class InitClient extends InitCommon
{
	private static final ResourceLocation texture = new ResourceLocation("umshops:textures/gui/icons.png");
	
	@Override
	void initSided()
	{
		TileEntityVetrinoRenderer vrend = new TileEntityVetrinoRenderer(-0.1F);
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityShop.class, vrend);
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAdminShop.class, vrend);
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityIndicator.class, new TileEntityIndicatorRenderer());
	}
	
	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent e)
	{
		if(e.phase == TickEvent.Phase.END)
		{
			TileEntityVetrinoRenderer.f1 += 0.16F;
			if(TileEntityVetrinoRenderer.f1 > 360F)
				TileEntityVetrinoRenderer.f1 = 0;
		}
	}
	
	@SubscribeEvent
	public void onRenderTick(TickEvent.RenderTickEvent e)
	{
		if(e.phase == TickEvent.Phase.END)
		{
			Minecraft mc = Minecraft.getMinecraft();
			if(mc.theWorld != null && !mc.skipRenderWorld && !mc.gameSettings.hideGUI && mc.currentScreen == null && !mc.gameSettings.showDebugInfo)
			{
				mc.renderEngine.bindTexture(texture);
				mc.ingameGUI.drawTexturedModalRect(1, 1, 0, 0, 6, 8);
				String gscStr = ClientEconomy.GSC.getGameString();
				int len = mc.fontRenderer.getStringWidth(gscStr);
				boolean drawRUR = ClientEconomy.RUR.getMoney() != 0.0d;
				if(drawRUR)
					mc.ingameGUI.drawTexturedModalRect(9+len+4, 1, 0, 9, 6, 8);
				mc.fontRenderer.drawStringWithShadow(gscStr, 9, 1, 0xffffff);
				if(drawRUR)
					mc.fontRenderer.drawStringWithShadow(ClientEconomy.RUR.getGameString(), 9+len+4+7, 1, 0xffffff);
			}
		}
	}
}
