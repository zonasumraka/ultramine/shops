package org.ultramine.mods.shops.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerVetrino extends Container
{
	private IInventory inv;

	public ContainerVetrino(IInventory player, IInventory inv, int mainslot)
	{
		this.inv = inv;

		addSlotToContainer(new Slot(inv, mainslot, 80, 17));

		for (int j = 0; j < 3; j++)
		{
			for (int i1 = 0; i1 < 9; i1++)
			{
				addSlotToContainer(new Slot(player, i1 + j * 9 + 9, 8 + i1 * 18, 84 + j * 18));
			}
		}

		for (int k = 0; k < 9; k++)
		{
			addSlotToContainer(new Slot(player, k, 8 + k * 18, 142));
		}
	}

	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return inv.isUseableByPlayer(entityplayer);
	}
	
	public ItemStack slotClick(int slot, int button, int op, EntityPlayer player)
	{
		if(slot == 0)
		{
			ItemStack is = player.inventory.getItemStack();
			if(is == null)
			{
				inv.setInventorySlotContents(0, null);
			}
			else
			{
				ItemStack copy = is.copy();
				copy.stackSize = 1;
				inv.setInventorySlotContents(0, copy);
			}
			return null;
		}
		else
		{
			return super.slotClick(slot, button, op, player);
		}
	}
	
	public ItemStack transferStackInSlot(EntityPlayer entity, int slotInd)
	{
		Slot slot = (Slot)this.inventorySlots.get(slotInd);

		if (slot != null && slot.getHasStack())
		{
			ItemStack is = slot.getStack();

			if (slotInd == 0)
			{
				inv.setInventorySlotContents(0, null);
			}
			else
			{
				ItemStack copy = is.copy();
				copy.stackSize = 1;
				inv.setInventorySlotContents(0, copy);
				return null;
			}

			if (is.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}
		}

		return null;
	}
}
