package org.ultramine.mods.shops.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.ultramine.gui.ElementButton;
import org.ultramine.gui.ElementLabel;
import org.ultramine.gui.ElementTextField;
import org.ultramine.gui.GuiStyled;
import org.ultramine.gui.GuiUtils;
import org.ultramine.gui.IGuiElement;
import org.ultramine.mods.shops.te.TileEntityShop;
import org.ultramine.server.util.InventoryUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static org.ultramine.util.I18n.tlt;

@SideOnly(Side.CLIENT)
public class GuiShopBuy extends GuiStyled
{
	private static final ResourceLocation texture = new ResourceLocation("UMShops:textures/gui/shopset.png");
	private static final NumberFormat dformat = new DecimalFormat("#0.00");
	
	private TileEntityShop shop;
	private EntityPlayer player;
	
	private int prodAmount = 1;
	private String msg;
	
	private ElementTextField tbProdAmount;
	private ElementButton buyBt;
	private ElementButton sellBt;
	
	private int playerprodcount;
	
	public GuiShopBuy(TileEntityShop shop, EntityPlayer player)
	{
		this.shop = shop;
		this. player = player;
		setSize(256, 166);
		setBG(texture);
		addElement(new ElementButton(0, 241, 6, 8, 8, "X"));
		
		tbProdAmount = new ElementTextField(3, 50, 70, 50, 12);
		tbProdAmount.setMaxStringLength(9);
		tbProdAmount.setText("1");
		tbProdAmount.setFilterString("0123456789");
		addElement(tbProdAmount);
		addElement(new ElementLabel(103, 72, tlt("ultramine.shop.guibuy.amountlabel")));
		addElement(buyBt = new ElementButton(1, 58, 138, 50, 20, tlt("ultramine.shop.guibuy.buy")));
		addElement(sellBt= new ElementButton(2, 148, 138, 50, 20, tlt("ultramine.shop.guibuy.sell")));
		
		addElement(new ElementButton(11, 10, 70, 12, 12, "1"));
		addElement(new ElementButton(12, 23, 70, 12, 12, "16"));
		addElement(new ElementButton(13, 36, 70, 12, 12, "64"));
	}
	
	@Override
	public void actionPerformed(int id, IGuiElement el, Object... data)
	{
		msg = null;
		
		switch(id)
		{
		case 0:
			Minecraft.getMinecraft().thePlayer.closeScreen();
			return;

		case 1:
			shop.sendBuySellCommand(5, prodAmount);
			return;
			
		case 2:
			shop.sendBuySellCommand(6, prodAmount);
			return;
		case 3:
			prodAmount = tbProdAmount.getAsIntegerOr(0);
			return;
		
		case 11:
			tbProdAmount.setText("1");
			tbProdAmount.addStr(""); //Notify listeners
			return;
		case 12:
			tbProdAmount.setText("16");
			tbProdAmount.addStr(""); //Notify listeners
			return;
		case 13:
			tbProdAmount.setText("64");
			tbProdAmount.addStr(""); //Notify listeners
			return;
		}
	}
	
	@Override
	public void onGuiClosed()
	{
		
	}
	
	@Override
	public void update()
	{
		super.update();
		playerprodcount = 0;
		buyBt.enabled = shop.getProduct() != null && shop.canSell(1);
		sellBt.enabled = shop.getProduct() != null && shop.canBuy(1);
		if(shop.getProduct() != null)
			playerprodcount = InventoryUtil.countItems(player.inventory, shop.getProduct());
	}

	@Override
	protected void drawForeground(int mx, int my)
	{
		mc.fontRenderer.drawString(tlt("ultramine.shop.guibuy.name"), guiLeft+10, guiTop+6, 0x404040);
		
		ItemStack product = shop.getProduct();
		String tovNme = tlt("ultramine.shop.guibuy.none");
		
		if(product != null)
		{
			int posX = guiLeft + 120;
			int posY = guiTop + 30;
			tovNme = product.getItem().getItemStackDisplayName(product);
			GuiUtils.drawCenteredString("\u00a7c" + tovNme, guiLeft + 128, guiTop + 20, 0x404040);
			RenderHelper.enableGUIStandardItemLighting();
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240, 240);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			
			GuiUtils.itemRenderer.renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, product, posX, posY);
			GuiUtils.itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, product, posX, posY);
			
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
			RenderHelper.disableStandardItemLighting();
		}
		
		if(shop.canSell(1))
			drawString(12, 50, tlt("ultramine.shop.guibuy.selllabel", dformat.format(shop.getCurrentSellPrice()), shop.getMaxAmountForSell(), shop.getClientMoneySign()));
		if(shop.canBuy(1))
			drawString(12, 60, tlt("ultramine.shop.guibuy.buylabel", dformat.format(shop.getCurrentBuyPrice()), shop.getMaxAmountForBuy(), shop.getClientMoneySign()));
		if(!shop.canSell(1) && !shop.canBuy(1))
			drawString(12, 55, tlt("ultramine.shop.guibuy.allnone"));
		
		{ //UNDER TEXTBOX
			drawString(10, 85, tlt("ultramine.shop.guibuy.yourmoney", dformat.format(shop.getClientMoney()), shop.getClientMoneySign()));
			drawString(10, 95, tlt("ultramine.shop.guibuy.yourprod", playerprodcount));
			
			if(prodAmount != 0)
			{
				if(shop.canSell(prodAmount))
					drawString(10, 105, tlt("ultramine.shop.guibuy.buycost", dformat.format(shop.getSellCost(prodAmount)), shop.getClientMoneySign()));
				if(shop.canBuy(prodAmount))
					drawString(10, 115, tlt("ultramine.shop.guibuy.sellsales", dformat.format(shop.getBuyCost(prodAmount)), shop.getClientMoneySign()));
			}
			if(msg != null && !msg.equals(""))
				drawString(10, 130, msg);
		}
		
		if(product != null)
		{
			int posX = guiLeft + 120;
			int posY = guiTop + 30;
			if(mx > posX && mx < posX+16 && my > posY && my < posY+16)
			{
				GuiUtils.drawToolTip(product, mx + 8, my + 8, width, height);
			}
		}
	}
	
	public void acceptMessage(int id)
	{
		switch(id)
		{
		case 1: msg = tlt("ultramine.shop.guibuy.msg.nomoney");		break;
		case 2: msg = tlt("ultramine.shop.guibuy.msg.noprodbuy");	break;
		case 3: msg = tlt("ultramine.shop.guibuy.msg.noplaces");	break;
		case 5: msg = tlt("ultramine.shop.guibuy.msg.noprodsell");	break;
		case 6: msg = tlt("ultramine.shop.guibuy.msg.shopnomoney");	break;
		}
	}
}
