package org.ultramine.mods.shops.blocks;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import org.ultramine.mods.shops.te.TileEntityAdminShop;
import org.ultramine.mods.shops.te.TileEntityShop;

import java.util.ArrayList;
import java.util.List;

public class BlockShop extends BlockContainer
{
	@SideOnly(Side.CLIENT)
	private IIcon icon1;
	
	public BlockShop()
	{
		super(Material.iron);
		setBlockUnbreakable();
		setResistance(6000000F);
		setStepSound(Block.soundTypeMetal);
		setCreativeTab(CreativeTabs.tabDecorations);
		setBlockName("um_shop");
		setBlockTextureName("umshops:shop");
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister reg)
	{
		super.registerBlockIcons(reg);
		icon1 = reg.registerIcon("umshops:shop1");
	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		if(meta < 2)
			return super.getIcon(meta, side);
		else
			return icon1;
	}
	
	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubBlocks(Item item, CreativeTabs tabs, List list)
	{
		list.add(new ItemStack(item, 1, 0));
		list.add(new ItemStack(item, 1, 1));
//		list.add(new ItemStack(item, 1, 2));
//		list.add(new ItemStack(item, 1, 3));
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		if(meta == 0)
			return new TileEntityShop();
		else if(meta == 1)
			return new TileEntityAdminShop();
		
		return null;
	}
	
	@Override
	public int damageDropped(int data)
	{
		return data;
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
	{
		TileEntityShop te = (TileEntityShop) world.getTileEntity(x, y, z);
		if(te != null)
		{
			if(world.isRemote)
			{
				return te.activateClient(player);
			}
			else /**/ if(FMLCommonHandler.instance().getSide().isServer())
			{
				return te.activateServer(player);
			}
		}
		
		return true;
	}
	
	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		ret.add(new ItemStack(Item.getItemFromBlock(this), 1, metadata));
		TileEntityShop te = (TileEntityShop) world.getTileEntity(x, y, z);
		if(te != null)
		{
			for(ItemStack is : te.inventory.getContents())
			{
				if(is != null)
					ret.add(is);
			}
		}
		return ret;
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack is)
	{
		if(entity instanceof EntityPlayer)
		{
			TileEntityShop te = (TileEntityShop) world.getTileEntity(x, y, z);
			if(te != null)
				te.setOwner(((EntityPlayer)entity).getGameProfile());
		}
	}
}
